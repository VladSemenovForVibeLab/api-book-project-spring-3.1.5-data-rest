package ru.svf.bookProject;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.svf.bookProject.entity.BookRepository;

@SpringBootApplication
@RequiredArgsConstructor
public class BookProjectApplication implements CommandLineRunner {
	private final BookRepository bookRepo;

	public static void main(String[] args) {
		SpringApplication.run(BookProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}
