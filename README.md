# bookProject

bookProject - это приложение для управления книгами и авторами, разработанное с использованием Spring Boot.

## Технологии

- Java 17
- Spring Boot 3.1.5
- Springdoc OpenAPI (Swagger) 2.2.0
- Spring Data JPA
- Spring Data REST
- Spring Web
- Spring Boot DevTools
- PostgreSQL
- Lombok
- Spring Boot Starter Test
- Maven

## Зависимости

В файле pom.xml вы найдете зависимости Maven, необходимые для вашего проекта

## Настройка базы данных

Для использования PostgreSQL, убедитесь, что у вас установлен и запущен PostgreSQL. В файле application.properties добавьте следующую конфигурацию:

### properties
```
spring.datasource.url=jdbc:postgresql://localhost:5432/yourdatabasename
spring.datasource.username=yourusername
spring.datasource.password=yourpassword
```

## Сборка и запуск

Для сборки проекта используйте Maven:
```
bash
mvn clean install
```
Для запуска приложения:
```
bash
java -jar target/book_project.jar
```
## Использование Swagger

После запуска приложения, вы можете получить доступ к Swagger API документации по адресу:

http://localhost:8080/swagger-ui.html

### Примеры

#### Создание книги
![img](docs/img.png)
#### Получение всех книг
![img](docs/img_1.png)
#### Создание автора
![img](docs/img_2.png)
#### Получение всех авторов
![img](docs/img_3.png)
#### Изменение данных автора
![img](docs/img_4.png)
#### Получение автора по id
![img](docs/img_5.png)
#### Удаление автора по id
![img](docs/img_6.png)
#### Добавление автора по id книги
![img](docs/img_7.png)
#### Изменение поля сущности книги
![img](docs/img_8.png)
#### Получение авторов книги по id книги
![img](docs/img_9.png)

## Вклад

Спасибо за использование bookProject. Если у вас есть предложения или найдены ошибки, пожалуйста, свяжитесь со мной.
```
ooovladislavchik@gmail.com
```